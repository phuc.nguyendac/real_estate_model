from flask import Flask, request
import json
import pickle
from model import run_model

app = Flask(__name__)


@app.route('/apiv2/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        decode = request.data.decode('utf-8')
        object_ = json.loads(decode)
        result = run_model(object_)
        return str(float(result))

if __name__ == "__main__":
    app.run(host='0.0.0.0')


